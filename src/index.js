/* eslint no-return-assign: 0 */
const
/**
 * A composable reduce function. Allows a cleaner syntax when squeezing in
 *     and when transforming array before reducing
 * @param  {Array} x What we want to reduce
 * @param  {func} f How we want to reduce
 * @param {Number|Object|String} i Initial value to pass to reducer
 * @return {Array|Number|Object|String} f(x) or value reduced from array
 */
  creduce = (x, f, i) => {
    return x.reduce(f, i);
  },

/**
 * Adds props to objects, key and value pairs both match
 * @param  {Number|String} x Value we want added as props
 * @param {Object} y Object we want to modify
 * @return {Object} y[x: x] or Object with keys and values mapped to one value
 */
  populateObj = (x, y) => {
    x[y] = y;
    return x;
  };

/**
 * Tests if arrays are equal
 * @param  {Array} x first array
 * @param  {Array} y second array
 * @param  {String} msg tap output message for assertion
 * @return {void}
 */
export default function (x, y, msg) {
  this.deepEqual(creduce(x, populateObj, {}),
      creduce(y, populateObj, {}), msg);
}
