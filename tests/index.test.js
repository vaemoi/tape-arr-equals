/* eslint no-magic-numbers: 0 */
import addAssertions from 'extend-tape';
import arrEquals from '..';
import tape from 'tape';

const test = addAssertions(tape, {arrEquals});

test(`------------ tape-arr-equals module test ------------`, (swear) => {
  swear.plan(2);
  swear.comment(`---- tape-arr-equals module test: arrEq ----`);
  swear.arrEquals([0, 1, 2, 3], [0, 1, 2, 3], `equal arrays are equal`);
  swear.arrEquals([0, 1, 2, 3], [1, 2, 3, 0], `different order is equal`);
});
